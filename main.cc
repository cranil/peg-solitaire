#include <cinttypes>
#include <cstdio>
#include <unordered_map>

#include "peg_solitaire.h"
namespace {
using namespace peg_solitaire;

static std::unordered_map<std::bitset<49>, uint64_t> hist;

uint64_t dfs(const board_t<7> &board, const state_t<7> &curr_state) {
  using namespace std;
  uint64_t soln_count = 0;
  int n_moves = 0;
  auto iter = hist.find(curr_state.m_state);
  if (iter != std::end(hist)) {
    auto v = *iter;
    soln_count = v.second;
  } else {
    for (auto &&move : board.moves) {
      if (curr_state.at(move.from.first, move.from.second) &&
          curr_state.at(move.over.first, move.over.second) &&
          !curr_state.at(move.to.first, move.to.second)) {
        auto next_state = apply_move(board, curr_state, move);
        n_moves++;
        soln_count += dfs(board, next_state);
      }
    }
    if (n_moves == 0) {
      if (curr_state.m_state.count() == 1) {
        soln_count = 1;
      }
    }
    hist[curr_state.m_state] = soln_count;
  }
  return soln_count;
}

}  // namespace
int main() {
  hist.reserve(1'073'741'824);
  using namespace peg_solitaire;
  // clang-format off
  board_t<7> board(
       "0011100"
       "0011100"
       "1111111"
       "1111111"
       "1111111"
       "0011100"
       "0011100"
  );

  state_t<7> init_state(
       "0011100"
       "0011100"
       "1111111"
       "1110111"
       "1111111"
       "0011100"
       "0011100"
  );
  // clang-format on
  std::printf("total possible moves: %zu\n", board.moves.size());
  auto n_solns = dfs(board, init_state);
  std::printf("total solutions: %'" PRIu64 "\n", n_solns);
  std::printf("%zu\n", hist.size());
  std::printf("\n");
}