#include "peg_solitaire.h"

#include <cinttypes>
#include <cstdio>

using namespace peg_solitaire;

state_t<7> apply_move(const board_t<7> &board, const state_t<7> &init_state,
                      const move_t move) {
  auto next_state = init_state;
  next_state.at(move.from.first, move.from.second) = false;
  next_state.at(move.over.first, move.over.second) = false;
  next_state.at(move.to.first, move.to.second) = true;
  return next_state;
}
