#pragma once
#include <bitset>
#include <vector>

namespace peg_solitaire {
struct move_t {
  std::pair<int, int> from, over, to;
};

template <int SIZE>
struct state_t {
  std::bitset<SIZE * SIZE> m_state;
  static constexpr int BOARD_SIZE = SIZE;

  explicit state_t(const char *l) : m_state(l) {}
  explicit state_t(const std::string *l) : m_state(l) {}

  auto at(const int i, const int j) const {
    return m_state[SIZE - 1 - j + SIZE * (SIZE - 1 - i)];
  }
  auto at(const int i, const int j) {
    return m_state[SIZE - 1 - j + SIZE * (SIZE - 1 - i)];
  }
};

template <int SIZE>
struct board_t {
  const std::bitset<SIZE * SIZE> holes;
  const std::vector<move_t> moves;
  static constexpr int BOARD_SIZE = SIZE;
  explicit board_t(const char *l) : holes(l), moves(find_moves()) {}
  explicit board_t(const std::string l) : holes(l), moves(find_moves()) {}

  auto at(const int i, const int j) const {
    return holes[SIZE - 1 - j + SIZE * (SIZE - 1 - i)];
  }
  void print(const state_t<7> &state, FILE *f = stdout) const {
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        if (at(i, j)) {
          if (state.at(i, j))
            std::fprintf(f, " 1");
          else
            std::fprintf(f, " 0");
        } else {
          std::fprintf(f, "  ");
        }
      }
      std::fprintf(f, "\n");
    }
  }

  std::vector<move_t> find_moves() {
    std::vector<move_t> moves;
    for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
        if (at(i, j)) {
          if (i - 2 >= 0)
            if (at(i - 2, j) && at(i - 1, j))
              moves.emplace_back(move_t{{i, j}, {i - 1, j}, {i - 2, j}});

          if (j - 2 >= 0)
            if (at(i, j - 2) && at(i, j - 1))
              moves.emplace_back(move_t{{i, j}, {i, j - 1}, {i, j - 2}});

          if (i + 2 < SIZE)
            if (at(i + 2, j) && at(i + 1, j))
              moves.emplace_back(move_t{{i, j}, {i + 1, j}, {i + 2, j}});

          if (j + 2 < SIZE)
            if (at(i, j + 2) && at(i, j + 1))
              moves.emplace_back(move_t{{i, j}, {i, j + 1}, {i, j + 2}});
        }
      }
    }
    return moves;
  }
};

template <int SIZE>
state_t<SIZE> apply_move(const board_t<SIZE> &board,
                         const state_t<SIZE> &init_state, const move_t move) {
  auto next_state = init_state;
  next_state.at(move.from.first, move.from.second) = false;
  next_state.at(move.over.first, move.over.second) = false;
  next_state.at(move.to.first, move.to.second) = true;
  return next_state;
}

template <int SIZE>
state_t<SIZE> unapply_move(const board_t<SIZE> &board,
                           const state_t<SIZE> &init_state, const move_t move) {
  auto next_state = init_state;
  next_state.at(move.from.first, move.from.second) = true;
  next_state.at(move.over.first, move.over.second) = true;
  next_state.at(move.to.first, move.to.second) = false;
  return next_state;
}

}  // namespace peg_solitaire